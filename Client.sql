CREATE TABLE MANAGER ( id INT PRIMARY KEY, Manager_name text, Manager_location text );

CREATE TABLE CONTRACT ( id INT PRIMARY KEY, Estimated_cost INT, Completion_date DATE );

CREATE TABLE STAFF ( id INT PRIMARY KEY, Staff_name text, Staff_location text );

CREATE TABLE CLIENT 
( Client_id INT, Name TEXT, 
Location TEXT, 
Manager_id INT, 
Contract_id INT, 
Staff_id INT, 
CONSTRAINT P_KEY PRIMARY KEY(Client_id), 
CONSTRAINT F_KEY_Mgr FOREIGN KEY(Manager_id) REFERENCES MANAGER(id), 
CONSTRAINT F_KEY_Con FOREIGN KEY(Contract_id) REFERENCES CONTRACT(id), 
CONSTRAINT F_KEY_Stf FOREIGN KEY(Staff_id) REFERENCES STAFF(id) );