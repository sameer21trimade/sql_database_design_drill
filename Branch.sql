CREATE TABLE Branch_Addr ( id int primary key, Branch_Addr text);

CREATE TABLE ISBN ( id int primary key, ISBN char(13), Title text, Author text, Publisher text);

CREATE TABLE BRANCH ( Branch_id int, 
Branch_Addr_id int, 
ISBN int, Num_copies int, 
CONSTRAINT P_KEY PRIMARY KEY(Branch_id), 
CONSTRAINT F_KEY_Addr FOREIGN KEY(Branch_Addr_id) REFERENCES Branch_Addr(id), 
CONSTRAINT F_KEY_ISBN FOREIGN KEY(ISBN) REFERENCES ISBN(id)  );
