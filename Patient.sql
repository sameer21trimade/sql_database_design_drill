CREATE TABLE PRESCRIPTION 
( id INT Primary Key,
 Drug TEXT,
 Date DATE,
 Dosage INT );

 CREATE TABLE DOCTOR
( id INT PRIMARY KEY,
  Doctor TEXT,
  Secretary TEXT );

CREATE TABLE PATIENT 
( Patient_id INT, 
Name TEXT, 
DOB DATE, 
Address TEXT, 
Prescription_id INT, 
Doctor_id INT, 
CONSTRAINT P_KEY PRIMARY KEY(Patient_id), 
CONSTRAINT F_KEY_Prsc FOREIGN KEY(Prescription_id) REFERENCES PRESCRIPTION(id), 
CONSTRAINT F_KEY_Doc FOREIGN KEY(Doctor_id) REFERENCES DOCTOR(id) );